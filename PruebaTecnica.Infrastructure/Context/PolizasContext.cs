﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Models;

namespace PruebaTecnica.Infrastructure.Context
{
    public class PolizasContext : DbContext
    {
        public PolizasContext(DbContextOptions<PolizasContext> options) : base(options)
        {
        }

        public DbSet<Poliza> Poliza { get; set; }
        public DbSet<Ciudad> Ciudad { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}