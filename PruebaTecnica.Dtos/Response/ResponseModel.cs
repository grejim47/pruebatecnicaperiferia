﻿namespace PruebaTecnica.Dtos.Response
{
    public class ResponseModel<T>
    {
        public bool Success { get; set; }
        public string Description { get; set; }
        public T Data { get; set; }
        public ResponseModel(bool Success, string Description, T Data)
        {
            this.Success = Success;
            this.Description = Description;
            this.Data = Data;
        }

    }
}
