﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Request
{
    public class UpdateCiudadRequest : IRequest<ResponseModel<Ciudad>>
    {
        public string Ciudad { get; set; }
    }
}
