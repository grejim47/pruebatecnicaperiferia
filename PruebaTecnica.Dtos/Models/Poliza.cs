﻿using System.ComponentModel.DataAnnotations;

namespace PruebaTecnica.Dtos.Models
{
    public class Poliza
    {
        [Key]
        public int Id { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string Placa { get; set; }
        public int CiudadVenta { get; set; }

        public Poliza()
        {
        }

        public Poliza(string placa, int ciudadVenta)
        {
            FechaInicio = DateTime.Now;
            FechaFin = DateTime.Now.AddMonths(1);
            FechaVencimiento = DateTime.Now.AddMinutes(2);//Vencimiento corto para validar req
            Placa = placa.ToUpper();
            CiudadVenta = ciudadVenta;
        }
    }
}
