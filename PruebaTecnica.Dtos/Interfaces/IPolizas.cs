﻿using PruebaTecnica.Dtos.Models;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface IPolizas
    {
        Task<List<Poliza>> GetPoliza();
        Task<Poliza> GetPolizaByPlaca(string idplaca);
        Task<List<Ciudad>> GetCiudades();
        Task AddPoliza(Poliza poliza);
        Task AddCiudad(Ciudad ciudad);
        Task UpdatePoliza(Poliza poliza);
        Task UpdateCiudad(Ciudad ciudad);
    }
}
