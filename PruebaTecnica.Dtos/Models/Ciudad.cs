﻿using System.ComponentModel.DataAnnotations;

namespace PruebaTecnica.Dtos.Models
{
    public class Ciudad
    {
        [Key]
        public int Id { get; set; }
        public string NombreCiudad { get; set; }
        public bool Activa { get; set; }

        public Ciudad(string nombreCiudad)
        {
            NombreCiudad = nombreCiudad;
            Activa = true;
        }

        public Ciudad()
        {
        }
    }
}
