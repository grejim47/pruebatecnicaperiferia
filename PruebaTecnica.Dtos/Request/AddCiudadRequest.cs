﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Request
{
    public class AddCiudadRequest : IRequest<ResponseModel<Ciudad>>
    {
        public string nombreCiudad { get; set; }
    }
}
