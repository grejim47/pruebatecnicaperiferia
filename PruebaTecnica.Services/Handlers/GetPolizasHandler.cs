﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;

namespace PruebaTecnica.Services.Handlers
{
    public class GetPolizasHandler : IRequestHandler<GetPolizaRequest, List<Poliza>>
    {
        private readonly IAplication _app;

        public GetPolizasHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<List<Poliza>> Handle(GetPolizaRequest request, CancellationToken cancellationToken) => await _app.GetPolizas();
    }
}
