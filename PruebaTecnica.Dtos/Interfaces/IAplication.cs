﻿using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Interfaces
{
    public interface IAplication
    {
        Task<List<Poliza>> GetPolizas();
        Task<ResponseModel<Ciudad>> AddCiudad(AddCiudadRequest request);
        Task<ResponseModel<Ciudad>> UpdateCiudad(UpdateCiudadRequest request);
        Task<ResponseModel<Poliza>> GetPolizaByPlaca(GetPolizaByPlacaRequest request);
        Task<ResponseModel<Poliza>> AddPoliza(AgregarPolizaRequest request);
    }
}
