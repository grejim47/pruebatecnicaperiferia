﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Request
{
    public class AgregarPolizaRequest : IRequest<ResponseModel<Poliza>>
    {
        public string Placa { get; set; }
        public string Ciudad { get; set; }
    }
}
