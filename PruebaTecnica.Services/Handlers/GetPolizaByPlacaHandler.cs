﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Services.Handlers
{
    public class GetPolizaByPlacaHandler : IRequestHandler<GetPolizaByPlacaRequest, ResponseModel<Poliza>>
    {
        private readonly IAplication _app;

        public GetPolizaByPlacaHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseModel<Poliza>> Handle(GetPolizaByPlacaRequest request, CancellationToken cancellationToken) => await _app.GetPolizaByPlaca(request);
    }
}
