﻿using MediatR;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Dtos.Request
{
    public class GetPolizaByPlacaRequest : IRequest<ResponseModel<Poliza>>
    {
        public string Placa { get; set; }
    }
}
