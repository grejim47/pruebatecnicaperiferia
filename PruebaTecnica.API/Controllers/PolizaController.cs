﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolizaController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PolizaController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        [Route("GetPolizas")]
        public async Task<List<Poliza>> GetPolizas() => await _mediator.Send(new GetPolizaRequest());
        [HttpPost]
        [Route("GetPolizaByPlaca")]
        public async Task<ResponseModel<Poliza>> GetPolizaByPlaca(GetPolizaByPlacaRequest request) => await _mediator.Send(request);
        [HttpPost]
        [Route("AddPoliza")]
        public async Task<ResponseModel<Poliza>> AddPoliza(AgregarPolizaRequest request) => await _mediator.Send(request);
        [HttpPost]
        [Route("AddCiudad")]
        public async Task<ResponseModel<Ciudad>> AddCiudad(AddCiudadRequest request) => await _mediator.Send(request);
        [HttpPost]
        [Route("UpdateCiudad")]
        public async Task<ResponseModel<Ciudad>> UpdateCiudad(UpdateCiudadRequest request) => await _mediator.Send(request);
    }
}
