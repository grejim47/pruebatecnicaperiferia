﻿using MediatR;
using PruebaTecnica.Dtos.Models;

namespace PruebaTecnica.Dtos.Request
{
    public class GetPolizaRequest : IRequest<List<Poliza>>
    {
    }
}
