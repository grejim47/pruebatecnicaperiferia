﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Services.Handlers
{
    public class AgregarPolizaHandler : IRequestHandler<AgregarPolizaRequest, ResponseModel<Poliza>>
    {
        private readonly IAplication _app;

        public AgregarPolizaHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseModel<Poliza>> Handle(AgregarPolizaRequest request, CancellationToken cancellationToken) => await _app.AddPoliza(request);
    }
}
