﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Services.Handlers
{
    public class AddCiudadHandler : IRequestHandler<AddCiudadRequest, ResponseModel<Ciudad>>
    {
        private readonly IAplication _app;

        public AddCiudadHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseModel<Ciudad>> Handle(AddCiudadRequest request, CancellationToken cancellationToken) => await _app.AddCiudad(request);
    }
}
