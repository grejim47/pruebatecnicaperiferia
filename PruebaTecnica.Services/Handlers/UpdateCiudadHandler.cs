﻿using MediatR;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Services.Handlers
{
    public class UpdateCiudadHandler : IRequestHandler<UpdateCiudadRequest, ResponseModel<Ciudad>>
    {
        private readonly IAplication _app;

        public UpdateCiudadHandler(IAplication app)
        {
            _app = app;
        }

        public async Task<ResponseModel<Ciudad>> Handle(UpdateCiudadRequest request, CancellationToken cancellationToken) => await _app.UpdateCiudad(request);
    }
}
