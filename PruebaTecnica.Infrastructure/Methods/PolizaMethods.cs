﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Infrastructure.Context;

namespace PruebaTecnica.Infrastructure.Methods
{
    public class PolizaMethods : IPolizas
    {
        private readonly PolizasContext _context;

        public PolizaMethods(PolizasContext context)
        {
            _context = context;
        }

        public async Task<List<Poliza>> GetPoliza() => await _context.Poliza.ToListAsync();
        public async Task<Poliza> GetPolizaByPlaca(string idplaca) => await _context.Poliza.Where(x => x.Placa == idplaca).FirstOrDefaultAsync();
        public async Task<List<Ciudad>> GetCiudades() => await _context.Ciudad.ToListAsync();
        public async Task AddPoliza(Poliza poliza)
        {
            await _context.Poliza.AddAsync(poliza);
            await _context.SaveChangesAsync();
        }
        public async Task AddCiudad(Ciudad ciudad)
        {
            await _context.Ciudad.AddAsync(ciudad);
            await _context.SaveChangesAsync();
        }
        public async Task UpdatePoliza(Poliza poliza)
        {
            _context.Poliza.Update(poliza);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateCiudad(Ciudad ciudad)
        {
            _context.Ciudad.Update(ciudad);
            await _context.SaveChangesAsync();
        }
    }
}
