﻿using PruebaTecnica.Dtos.Interfaces;
using PruebaTecnica.Dtos.Models;
using PruebaTecnica.Dtos.Request;
using PruebaTecnica.Dtos.Response;

namespace PruebaTecnica.Application.Concrete
{
    public class ApplicationConcrete : IAplication
    {
        private readonly IPolizas _polizasInfrastructure;

        public ApplicationConcrete(IPolizas almacen)
        {
            _polizasInfrastructure = almacen;
        }
        public async Task<List<Poliza>> GetPolizas() => await _polizasInfrastructure.GetPoliza();
        public async Task<ResponseModel<Ciudad>> AddCiudad(AddCiudadRequest request)
        {
            ResponseModel<Ciudad> result;
            var Ciudades = await _polizasInfrastructure.GetCiudades();
            var existe = Ciudades.Where(x => x.NombreCiudad == request.nombreCiudad.ToUpper()).Any();
            if (existe)
            {
                result = new ResponseModel<Ciudad>(false, "Ya existe la ciudad", null);
                return result;
            }
            var Ciudad = new Ciudad(request.nombreCiudad.ToUpper());
            await _polizasInfrastructure.AddCiudad(Ciudad);
            return new ResponseModel<Ciudad>(true, "Exitoso", Ciudad);
        }
        public async Task<ResponseModel<Ciudad>> UpdateCiudad(UpdateCiudadRequest request)
        {
            ResponseModel<Ciudad> result;
            var Ciudades = await _polizasInfrastructure.GetCiudades();
            var ciudad = Ciudades.Where(x => x.NombreCiudad == request.Ciudad.ToUpper()).FirstOrDefault();
            if (ciudad == null)
                return new ResponseModel<Ciudad>(false, "No existe la ciudad", null);
            else
            {
                ciudad.Activa = !ciudad.Activa;
                await _polizasInfrastructure.UpdateCiudad(ciudad);
                return new ResponseModel<Ciudad>(true, "Exitoso", ciudad);
            }
        }
        public async Task<ResponseModel<Poliza>> GetPolizaByPlaca(GetPolizaByPlacaRequest request)
        {
            var Poliza = await _polizasInfrastructure.GetPolizaByPlaca(request.Placa.ToUpper());
            if (Poliza != null)
                return new ResponseModel<Poliza>(true, "Encontrado", Poliza);
            else
                return new ResponseModel<Poliza>(false, "No Encontrado", null);
        }
        public async Task<ResponseModel<Poliza>> AddPoliza(AgregarPolizaRequest request)
        {
            var Ciudades = await _polizasInfrastructure.GetCiudades();
            var Ciudad = Ciudades.Where(x => x.NombreCiudad == request.Ciudad.ToUpper()).FirstOrDefault();
            if (Ciudad == null)
                return new ResponseModel<Poliza>(false, "Ciudad no parametrizada", null);

            if (!Ciudad.Activa)
                return new ResponseModel<Poliza>(false, "Ciudad no permitida para vender polizas", null);

            var Poliza = await _polizasInfrastructure.GetPolizaByPlaca(request.Placa.ToUpper());
            if (Poliza != null)
            {
                if (Poliza.FechaVencimiento < DateTime.Now)
                    return new ResponseModel<Poliza>(false, $"No se puede vender seguro, supera fecha de vencimiento {Poliza.FechaVencimiento.ToString("yyyy/MM/dd")}", null);

                Poliza.FechaInicio = DateTime.Now;
                Poliza.FechaFin = DateTime.Now.AddMonths(1);
                Poliza.FechaVencimiento = DateTime.Now.AddMinutes(2);//Vencimiento corto para validar req
                Poliza.CiudadVenta = Ciudad.Id;
                await _polizasInfrastructure.UpdatePoliza(Poliza);
                return new ResponseModel<Poliza>(true, "Exitoso", Poliza);
            }
            else
            {
                Poliza = new Poliza(request.Placa, Ciudad.Id);
                await _polizasInfrastructure.AddPoliza(Poliza);
                return new ResponseModel<Poliza>(true, "Exitoso", Poliza);
            }

        }

    }
}
